<?php

/**
 * @file
 *   Drush commands for ebt_code_complete.
 */

/**
 * Implements of hook_drush_command().
 */
function ebt_code_complete_drush_command() {
  $items = array();

  // the key in the $items array is the name of the command.
  $items['entity-code-complete'] = array(
    'callback' => 'drush_entity_code_complete',
    'description' => dt("Generates ebt_code_complete.class.inc."),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('ecc'),
  );

  return $items;
}

/**
 * Implementats of hook_drush_help().
 */
function ebt_code_complete_drush_help($section) {
  switch ($section) {
    case 'drush:roundabout-plugin':
      return dt("Downloads the Roundabout plugin " . ROUNDABOUT_VERSION . " from github.com, default location is sites/all/libraries.");
  }
}

/**
 * Command to generate ebt_code_complete.class.inc.
 */
function drush_entity_code_complete() {
  module_load_include('inc', 'ebt_code_complete');

  $filepath = drupal_get_path('module', 'ebt_code_complete'). '/ebt_code_complete.class.inc';
  $code = ebt_code_complete_get_code();

  $fh = fopen($filepath, 'w');
  fwrite($fh, $code);
  fclose($fh);

  drush_print('File ebt_code_complete.class.inc has been generated!');
}
