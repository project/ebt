<?php

function ebt_code_complete_get_code() {
  $code = "<?php\n";
  $code .= "\n";
  $entitiy_types = field_info_bundles();
  foreach ($entitiy_types as $entity_type => $bundles) {
    $entity_properties = array();
    foreach ($bundles as $bundle => $bundle_info) {
      $entity_wrapper = entity_metadata_wrapper($entity_type, NULL, array('bundle' => $bundle));

      $bundle_properties = $entity_wrapper->getPropertyInfo();
      $entity_properties = array_merge($entity_properties, $bundle_properties);
      $code .= ebt_code_complete_get_class_code($entity_type, $bundle, $bundle_properties);
    }
    $code .= ebt_code_complete_get_class_code($entity_type, NULL, $entity_properties);    
  }

  return $code;
}

/**
 * Generates code for specific EntityMetadataWrapper class.
 */
function ebt_code_complete_get_class_code($entity_type, $bundle, $properties) {
  $class_name = ebt_code_complete_get_class_name($entity_type, $bundle);

  $code .= "/**\n";
  $code .= " * show off @property, @property-read, @property-write\n";
  $code .= " *\n";

  foreach ($properties as $property => $property_info) {
    $return_class_name = ebt_code_complete_get_class_name_by_info($property_info);
    $code .= " * @property $return_class_name \$$property\n";
  }

  $code .= " */\n";
  $code .= "class $class_name extends EntityDrupalWrapper\n";
  $code .= "{\n";
  $code .= "  /**\n";
  $code .= "   * @return $class_name\n";
  $code .= "   */\n";
  $code .= "  static function factory(\$data = NULL, array \$info = array())\n";
  $code .= "  {\n";
  //$code .= "    \$info['bundle'] = '$bundle';\n";
  $code .= "    return entity_metadata_wrapper('$entity_type', \$data, \$info);\n";
  $code .= "  }\n";
  $code .= "\n";
  $code .= "  /**\n";
  $code .= "   * @return $class_name\n";
  $code .= "   */\n";
  $code .= "  static function cast(\$wrapper)\n";
  $code .= "  {\n";
  $code .= "    return \$wrapper;\n";
  $code .= "  }\n";
  $code .= "}\n";
  $code .= "\n\n";

  return $code;
}

/**
 * Returns EbtWrapper class name by bundle.
 */
function ebt_code_complete_get_class_name($entity_type, $bundle = NULL) {
  $class_name = 'ebt_wrapper_' . $entity_type;
  if (!empty($bundle)) {
    $class_name .= '_' . $bundle;
  }

  // Capitalize words and remove '_'.
  return implode('', array_map('ucfirst', explode('_',  $class_name)));
}

/**
 * Returns EbtWrapper or EntityMetadataWrapper class name by property info.
 * 
 * See entity_metadata_wrapper().
 */
function ebt_code_complete_get_class_name_by_info($info) {
  $type = $info['type'];
  if ($type == 'entity' || (($entity_info = entity_get_info()) && isset($entity_info[$type]))) {
    return ebt_code_complete_get_class_name($type);
  }
  elseif ($type == 'list' || entity_property_list_extract_type($type)) {
    return 'EntityListWrapper';
  }
  elseif (isset($info['property info'])) {
    return 'EntityStructureWrapper';
  }
  else {
    return 'EntityValueWrapper';
  }
}
