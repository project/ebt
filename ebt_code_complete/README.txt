* Background

One of the serious problems when doing development with entity_metadata_wrapper
is a lack of the code complete feature in the IDE, because most of the methods are
virtual. This module solves this problem and allows to use code complete feature
for entity metadata wrappers in modern PHP editors such as NetBeans or PhpStorm.
This module also works nice with nested properties caused by the Entity reference.

* Usage

1. Generate ebt_code_complete.class.inc with a drush command:

  drush entity-code-complete
  drush ecc

2. Optional: include ebt_code_complete.class.inc in you project
only if you will use cast() or factory() methods:

  module_load_include('inc', 'ebt_code_complete', 'ebt_code_complete.class');

3. Define variable type using PHPDoc and use code complete in your IDE:

  /**
   * @var $pet_wrapper EbtWrapperNodePet
   */
  $pet = node_load(123);
  $pet_wrapper = entity_metadata_wrapper('node', $pet);
  $owner_birth_date = $pet_wrapper->field_pet_owner->field_birth_date->value()); // profit!

Alternatively you can use factory() method:

  $pet = node_load(123);
  $pet_wrapper = EbtWrapperNodePet::factory($pet);
  $owner_birth_date = $pet_wrapper->field_pet_owner->field_birth_date->value()); // profit!

Or cast() method:

  $pet = node_load(123);
  $pet_wrapper = entity_metadata_wrapper('node', $pet);
  $pet_wrapper = EbtWrapperNodePet::cast($pet_wrapper);
  $owner_birth_date = $pet_wrapper->field_pet_owner->field_birth_date->value()); // profit!

You can also use EbtWrapperNode class name instead of EbtWrapperNodePet. In this
case properties for all node types will be included.

* How it works

This modules generates additional entity metadata wrapper classes in
ebt_code_complete.class.inc file, which you can include an use in your projects.

These classes just extends EntityMetadaWrapper and they have all
virtual methods defined as PHPDoc comments, so it is possible to
use code completion for them!

Here is example of definition of this class:

  /**
   * show off @property, @property-read, @property-write
   *
   * @property EntityStructureWrapper $body
   * @property EntityListWrapper $field_tags
   * @property EntityStructureWrapper $field_image
   * @property EntityValueWrapper $nid
   *
   * ...
   *
   * @property EntityValueWrapper $comment_count_new
   */
  class EbtWrapperNodeArticle extends EntityDrupalWrapper
  {
    /**
     * @return EbtWrapperNodeArticle
     */
    static function factory($data = NULL, array $info = array())
    {
      return entity_metadata_wrapper('node', $data, $info);
    }

    /**
     * @return EbtWrapperNodeArticle
     */
    static function cast($wrapper)
    {
      return $wrapper;
    }
  }
